package com.equipo2.Appkademy.core.security.model;

public enum Role {

    USER,
    ADMIN,
    SUPER_ADMIN

}
